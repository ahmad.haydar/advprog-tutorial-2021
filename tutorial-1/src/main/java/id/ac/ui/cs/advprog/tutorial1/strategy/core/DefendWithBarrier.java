package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    public String getType(){
        return "defend with barrier";
    }
    public String defend(){
        return "you think i'm going to stand right there and take it";
    }
}
