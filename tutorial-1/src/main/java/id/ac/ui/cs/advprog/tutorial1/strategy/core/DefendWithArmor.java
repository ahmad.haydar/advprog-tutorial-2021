package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    public String getType(){
        return "defend with armor";
    }
    public String defend(){
        return "Go ahead! ATTACK ME!";
    }
}
