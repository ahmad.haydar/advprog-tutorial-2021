package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    public String getType(){
        return "attack with magic";
    }
    public String attack(){
        return "darker... yet darker";
    }
}
