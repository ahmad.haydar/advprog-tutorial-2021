package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    public String getType(){
        return "attack with sword";
    }
    public String attack(){
        return "Water surface slash!";
    }
}
