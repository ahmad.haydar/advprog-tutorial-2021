package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    public String getType(){
        return "defend with Shield";
    }
    public String defend(){
        return "ouch! that doesn't hurt";
    }
}
