package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    public String getType(){
        return "attack with gun";
    }
    public String attack(){
        return "Bang!Bang!Bang";
    }
}
